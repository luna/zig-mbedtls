const std = @import("std");

pub const c = @cImport({
    @cInclude("mbedtls/net_sockets.h");
    @cInclude("mbedtls/debug.h");
    @cInclude("mbedtls/ssl.h");
    @cInclude("mbedtls/entropy.h");
    @cInclude("mbedtls/ctr_drbg.h");
    @cInclude("mbedtls/error.h");
    @cInclude("mbedtls/certs.h");
});

// compatibility functions. see src/tls_compat.c
extern fn tls_compat_config_init() *c.mbedtls_ssl_config;
extern fn tls_compat_config_deinit(ptr: *c.mbedtls_ssl_config) void;

const log = std.log.scoped(.tls);

/// A representation of the MBedTLS context.
pub const Context = struct {
    allocator: *std.mem.Allocator,
    server_fd: c.mbedtls_net_context,
    entropy: c.mbedtls_entropy_context,
    ctr_drbg: c.mbedtls_ctr_drbg_context,
    ssl: c.mbedtls_ssl_context,
    cacert: c.mbedtls_x509_crt,
    conf: *c.mbedtls_ssl_config,

    const Self = @This();

    pub fn init(allocator: *std.mem.Allocator) !Self {
        var conf = tls_compat_config_init();

        c.mbedtls_debug_set_threshold(1);

        var server_fd: c.mbedtls_net_context = undefined;
        var entropy: c.mbedtls_entropy_context = undefined;
        var ctr_drbg: c.mbedtls_ctr_drbg_context = undefined;
        var ssl: c.mbedtls_ssl_context = undefined;
        var cacert: c.mbedtls_x509_crt = undefined;

        c.mbedtls_net_init(&server_fd);
        c.mbedtls_ssl_init(&ssl);
        c.mbedtls_x509_crt_init(&cacert);
        c.mbedtls_ctr_drbg_init(&ctr_drbg);
        c.mbedtls_ssl_config_init(conf);

        c.mbedtls_entropy_init(&entropy);
        var ret = c.mbedtls_ctr_drbg_seed(&ctr_drbg, c.mbedtls_entropy_func, &entropy, null, 0);
        if (ret != 0) {
            log.err("failed to seed rng. ret={d}", .{ret});
            return error.SeedFail;
        }

        const path = try std.cstr.addNullByte(allocator, "/etc/ssl/certs/ca-certificates.crt");
        defer allocator.free(path);

        ret = c.mbedtls_x509_crt_parse_file(&cacert, path.ptr);
        if (ret < 0) {
            log.err("failed to parse ca-certificates. ret=0x{x}", .{ret});
            return error.CrtFail;
        }

        return Self{
            .allocator = allocator,
            .server_fd = server_fd,
            .entropy = entropy,
            .ctr_drbg = ctr_drbg,
            .ssl = ssl,
            .cacert = cacert,
            .conf = conf,
        };
    }

    pub fn deinit(self: *Self) void {
        c.mbedtls_net_free(&self.server_fd);
        c.mbedtls_x509_crt_free(&self.cacert);
        c.mbedtls_ssl_free(&self.ssl);
        c.mbedtls_ctr_drbg_free(&self.ctr_drbg);
        c.mbedtls_entropy_free(&self.entropy);
        tls_compat_config_deinit(self.conf);
        self.allocator.destroy(self);
    }

    pub const ConnectError = error{
        ConnectFail,
        ConfigFail,
        SetupFail,
        HostnameFail,
        OutOfMemory,
    };

    pub fn connect(
        self: *Self,
        host: []const u8,
        port_num: u16,
    ) ConnectError!void {
        // need two because mbedtls does smth with the memory?
        // its also the reason why we don't free it
        const host_cstr = try std.cstr.addNullByte(self.allocator, host);
        defer self.allocator.free(host_cstr);

        const port_str = try std.fmt.allocPrint(self.allocator, "{d}", .{port_num});
        defer self.allocator.free(port_str);

        const port_cstr = try std.cstr.addNullByte(self.allocator, port_str);
        defer self.allocator.free(port_cstr);

        var ret = c.mbedtls_net_connect(&self.server_fd, host_cstr, port_cstr, c.MBEDTLS_NET_PROTO_TCP);
        if (ret != 0) {
            printLog("failed to connect (net): mbedtls error: {d} {s}", ret);
            return error.ConnectFail;
        }

        ret = c.mbedtls_ssl_config_defaults(
            self.conf,
            c.MBEDTLS_SSL_IS_CLIENT,
            c.MBEDTLS_SSL_TRANSPORT_STREAM,
            c.MBEDTLS_SSL_PRESET_DEFAULT,
        );
        if (ret != 0) {
            printLog("failed to connect (config): mbedtls error: {d} {s}", ret);
            return error.ConfigFail;
        }

        c.mbedtls_ssl_conf_authmode(self.conf, c.MBEDTLS_SSL_VERIFY_OPTIONAL);
        c.mbedtls_ssl_conf_ca_chain(self.conf, &self.cacert, null);
        c.mbedtls_ssl_conf_rng(self.conf, c.mbedtls_ctr_drbg_random, &self.ctr_drbg);
        c.mbedtls_ssl_conf_dbg(self.conf, myDebug, null);

        ret = c.mbedtls_ssl_setup(&self.ssl, self.conf);
        if (ret != 0) {
            printLog("failed to connect (setup): mbedtls error: {d} {s}", ret);
            return error.SetupFail;
        }

        ret = c.mbedtls_ssl_set_hostname(&self.ssl, host_cstr.ptr);
        if (ret != 0) {
            printLog("failed to connect (set hostname): mbedtls error: {d} {s}", ret);
            return error.HostnameFail;
        }

        c.mbedtls_ssl_set_bio(
            &self.ssl,
            &self.server_fd,
            c.mbedtls_net_send,
            c.mbedtls_net_recv,
            null,
        );
    }

    pub fn handshake(self: *Context) !void {
        while (true) {
            const ret = c.mbedtls_ssl_handshake(&self.ssl);

            // Warning
            // If this function returns something other than 0,
            // MBEDTLS_ERR_SSL_WANT_READ, MBEDTLS_ERR_SSL_WANT_WRITE,
            // MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS or MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS,
            // you must stop using the SSL context for reading or writing,
            // and either free it or call mbedtls_ssl_session_reset()
            // on it before re-using it for a new connection;
            // the current connection must be closed.

            switch (ret) {
                0 => return,
                c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
                c.MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS, c.MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS => unreachable, // we do not have async/crypto callbacks
                c.MBEDTLS_ERR_SSL_CLIENT_RECONNECT => unreachable, // we are not dtls
                else => {
                    printLog("failed to handshake: mbedtls error: {d} {s}", ret);
                    return error.HandshakeFail;
                },
            }
        }
    }

    pub fn verify(self: *Context) !void {
        const flags = c.mbedtls_ssl_get_verify_result(&self.ssl);

        if (flags != 0) {
            var buffer: [512]u8 = undefined;

            const bytes = c.mbedtls_x509_crt_verify_info(
                &buffer,
                @sizeOf(@TypeOf(buffer)),
                "",
                flags,
            );
            var msg = buffer[0..@intCast(usize, bytes)];

            log.err("verify failed: {s}", .{msg});
        }
    }

    // we can not have perfect error sets with mbedtls because there's a lot
    // of errors and i do not know which are read and which are write.
    //
    // the best i can do is those. if you receive a Fail, your ssl context
    // should be destroyed.
    pub const ReadError =
        error{ ReadFail, EndOfStream, Closed };
    pub const WriteError =
        error{WriteFail};

    pub const Writer = std.io.Writer(*Self, WriteError, write);
    pub const Reader = std.io.Reader(*Self, ReadError, read);

    pub fn reader(self: *Self) Reader {
        return .{ .context = self };
    }

    pub fn writer(self: *Self) Writer {
        return .{ .context = self };
    }

    pub fn write(self: *Self, data: []const u8) WriteError!usize {
        while (true) {
            const ret = c.mbedtls_ssl_write(&self.ssl, data.ptr, data.len);

            // TODO handle when ret < data.len

            if (ret > 0) return @intCast(usize, ret);
            // From the mbed TLS documentation:
            //
            // Warning
            // If this function returns something other than a non-negative value,
            // MBEDTLS_ERR_SSL_WANT_READ, MBEDTLS_ERR_SSL_WANT_WRITE,
            // MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS or MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS,
            // you must stop using the SSL context for reading or writing,
            // and either free it or call mbedtls_ssl_session_reset()
            // on it before re-using it for a new connection;
            // the current connection must be closed.

            switch (ret) {
                c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
                c.MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS, c.MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS => unreachable,
                else => {
                    printLog("failed to write: mbedtls error: {d} {s}", ret);
                    return error.WriteFail;
                },
            }
        }
    }

    pub fn read(self: *Self, buf: []u8) ReadError!usize {
        while (true) {
            const ret = c.mbedtls_ssl_read(&self.ssl, buf.ptr, buf.len);

            // from mbed TLS docs:
            //
            // If this function returns something other than a positive value,
            // MBEDTLS_ERR_SSL_WANT_READ, MBEDTLS_ERR_SSL_WANT_WRITE,
            // MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS, MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS
            // or MBEDTLS_ERR_SSL_CLIENT_RECONNECT, you must stop using the
            // SSL context for reading or writing, and either free it or call
            // mbedtls_ssl_session_reset() on it before re-using it for a new
            // connection; the current connection must be closed.

            if (ret == 0) {
                return error.EndOfStream;
            }

            if (ret > 0) return @intCast(usize, ret);

            switch (ret) {
                c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
                c.MBEDTLS_ERR_SSL_ASYNC_IN_PROGRESS, c.MBEDTLS_ERR_SSL_CRYPTO_IN_PROGRESS => unreachable, // we do not have async/crypto callbacks
                c.MBEDTLS_ERR_SSL_CLIENT_RECONNECT => unreachable, // we are not dtls
                else => {
                    printLog("failed to read: mbedtls error: {d}, {s}", ret);
                    return error.ReadFail;
                },
            }
        }
    }
};

fn printLog(comptime fmt: []const u8, retval: c_int) void {
    var error_buffer: [0x100]u8 = undefined;
    c.mbedtls_strerror(retval, &error_buffer, 0x100);
    var sentineled_buffer: [*:0]u8 = @ptrCast([*:0]u8, &error_buffer);
    log.err(fmt, .{ retval, std.mem.spanZ(sentineled_buffer) });
}

export fn myDebug(
    ctx: ?*c_void,
    level: c_int,
    file: [*c]const u8,
    line: c_int,
    str: [*c]const u8,
) callconv(.C) void {
    _ = ctx;
    _ = level;
    std.debug.warn("{s}:{d}: {s}", .{ std.mem.spanZ(file), line, std.mem.spanZ(str) });
}

pub const MaybeSecureInnerStream = union(enum) {
    tls: *Context,
    plaintext: std.net.Stream,
};

/// A stream that can either be secured with TLS or not.
pub const MaybeSecureStream = struct {
    inner_stream: MaybeSecureInnerStream,

    const Self = @This();

    pub fn init(inner_stream: MaybeSecureInnerStream) Self {
        return Self{ .inner_stream = inner_stream };
    }

    pub fn deinit(self: Self) void {
        switch (self.inner_stream) {
            .tls => |ctx| ctx.deinit(),
            .plaintext => |stream| stream.close(),
        }
    }

    const ReadError =
        std.net.Stream.Reader.Error || Context.Reader.Error;
    const WriteError =
        std.net.Stream.Writer.Error || Context.Writer.Error;

    pub const Writer = std.io.Writer(*Self, WriteError, write);
    pub const Reader = std.io.Reader(*Self, ReadError, read);

    pub fn reader(self: *Self) Reader {
        return .{ .context = self };
    }

    pub fn writer(self: *Self) Writer {
        return .{ .context = self };
    }

    pub fn read(self: *Self, buf: []u8) ReadError!usize {
        switch (self.inner_stream) {
            .tls => |tls_ctx| return try tls_ctx.read(buf),
            .plaintext => |stream| return try stream.reader().read(buf),
        }
    }

    pub fn write(self: *Self, buf: []const u8) WriteError!usize {
        switch (self.inner_stream) {
            .tls => |tls_ctx| return try tls_ctx.write(buf),
            .plaintext => |stream| return try stream.writer().write(buf),
        }
    }
};
