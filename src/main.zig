const std = @import("std");
const tls = @import("lib.zig");

comptime {
    _ = tls;
}
pub fn main() !void {
    std.log.info("test", .{});
}
